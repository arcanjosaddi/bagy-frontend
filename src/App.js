import React from 'react';
import Routes from './routers';

import './assets/styles/App.scss';

function App() {
  return (

    <div className="App">
      <Routes />
      <footer className="footer"></footer>
    </div>
  );
}

export default App;
