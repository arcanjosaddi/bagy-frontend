import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink ({
    uri: 'https://staging-dot-bagy-api.appspot.com/graphql'
});

const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        'x-auth-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjoxMDIsInJvbGUiOjEsImV4cCI6MTU5NjIyNTExNy4xNjF9.BzaPY9RrF_gU5_lCThHLsH8cb3sW-CXsuLItLuDCBWY'
      }
    }
  });

const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
});

export default client;

/*● Loja 1 (token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjoxMDIsInJvbGUiOjEsImV4cCI6MTU5NjIyNTExNy4xNjF9.BzaPY9RrF_gU5_lCThHLsH8cb3sW-CXsuLItLuDCBWY)​
● Loja 2 (token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjoxMDMsInJvbGUiOjEsImV4cCI6MTU5NjIyNTEzOS41MTN9.WAsQKYRwRn6JtL_c_8UazGAcg4FemmwYdgk7GtEg650​)
● Loja 3 (token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjoxMDQsInJvbGUiOjEsImV4cCI6MTU5NjIyNTI0MS40Nzh9.cCQkP1TYnr7P-nEslbDiwhKKp0a1pXxbThtfljZ8ZXY)​
● Loja 4 (token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjoxMDUsInJvbGUiOjEsImV4cCI6MTU5NjIyNTI1Ny42OTl9.AI7sgISsxiuA14UOeqdiyHqZUV1N1FdiDr5klv2aVpk)​
● Loja 5 (token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdG9yZUlkIjoxMDYsInJvbGUiOjEsImV4cCI6MTU5NjIyNTI5OC42MDZ9.sNKrx6lryNEOCyPqXHjhWI4zaWd-L15PtyncRYjiT6Y​)*/