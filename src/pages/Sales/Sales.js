import React from 'react';
import Helmet from 'react-helmet'

import MenuSidebar from '../../components/MenuSidebar/MenuSidebar.js'
import HeaderTop from '../../components/HeaderTop/HeaderTop.js';

export default function Sales() {
    return (
        <div className="container">
            <Helmet title="Vendas - Bagy" />
            <MenuSidebar />
            <section className="dashboard">
                <HeaderTop title="Vendas" />
            </section>
        </div>
    )
}