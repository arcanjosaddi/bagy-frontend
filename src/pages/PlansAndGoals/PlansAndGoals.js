import React from 'react';
import Helmet from 'react-helmet'

import MenuSidebar from '../../components/MenuSidebar/MenuSidebar.js'
import HeaderTop from '../../components/HeaderTop/HeaderTop.js';

export default function PlansAndGoals() {
    return (
        <div className="container">
            <Helmet title="Planos e Metas - Bagy" />
            <MenuSidebar/>
            <section className="dashboard">
                <HeaderTop title="Planos e Metas" />
            </section>
        </div>
    )
}