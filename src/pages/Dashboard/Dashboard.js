import React from 'react';
import Helmet from 'react-helmet';
import { useQuery } from '@apollo/client';

// Query GraphQL
import { STORE_NAME } from './../../services/graphql.js';

// SASS
import './dashboard.scss';

// Components
import MenuSidebar from './../../components/MenuSidebar/MenuSidebar.js';
import HeaderTop from './../../components/HeaderTop/HeaderTop.js';
import Card from './../../components/Card/Card.js';
import Graphic from './../../components/Graphic/Graphic.js';
import TotalSales from './../../components/TotalSales/TotalSales.js';
import Orders from './../../components/Orders/Orders.js';

export default function Dashboard() {
    const { loading, error, data } = useQuery(STORE_NAME);

    if(error) console.log(error);

    return (
        <div className="container">
            <Helmet title="Visão Geral - Bagy" />
            <MenuSidebar/>
            <section className="dashboard">
                <HeaderTop title="Visão Geral" />
                <div className="grid">
                    <div className="row row-four">
                        <Card title="Total de Lojas" value="7213" />
                        <Card title="Faturamento total" value="100.000,00" />
                        <Card 
                            title="Loja destaque" 
                            value={ loading ? 'Carregando...' : data.me.name } 
                            />
                        <Card title="Meta Mensal" value="110.000,00" />
                    </div>
                </div>
                <div className="grid">
                    <div className="row row-one">
                        <Graphic />
                    </div>
                </div>
                <div className="grid">
                    <div className="row row-two">
                        <TotalSales />
                        <Orders />
                    </div>
                </div>
            </section>
        </div>
    );
}