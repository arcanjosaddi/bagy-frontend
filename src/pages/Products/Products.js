import React from 'react';
import Helmet from 'react-helmet'

import MenuSidebar from '../../components/MenuSidebar/MenuSidebar.js'
import HeaderTop from '../../components/HeaderTop/HeaderTop.js';

export default function Products() {
    return (
        <div className="container">
            <Helmet title="Produtos - Bagy" />
            <MenuSidebar/>
            <section className="dashboard">
                <HeaderTop title="Produtos" />
            </section>
        </div>
    )
}