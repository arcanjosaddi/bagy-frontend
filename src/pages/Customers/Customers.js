import React from 'react';
import Helmet from 'react-helmet'

import MenuSidebar from '../../components/MenuSidebar/MenuSidebar.js'
import HeaderTop from '../../components/HeaderTop/HeaderTop.js';

export default function Customers() {
    return (
        <div className="container">
            <Helmet title="Clientes - Bagy" />
            <MenuSidebar />
            <section className="dashboard">
                <HeaderTop title="Clientes" />
            </section>
        </div>
    )
}