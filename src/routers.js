import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Dashboard from './pages/Dashboard/Dashboard.js';
import Stores from './pages/Stores/Stores.js';
import Sales from './pages/Sales/Sales.js';
import Customers from './pages/Customers/Customers.js';
import Products from './pages/Products/Products.js';
import Settings from './pages/Settings/Settings.js';
import PlansAndGoals from './pages/PlansAndGoals/PlansAndGoals.js';

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={ Dashboard } />
                <Route path="/lojas" component={ Stores } />
                <Route path="/vendas" component={ Sales } />
                <Route path="/clientes" component={ Customers } />
                <Route path="/produtos" component={ Products } />
                <Route path="/planos-e-metas" component={ PlansAndGoals } />
                <Route path="/configuracoes" component={ Settings } />
            </Switch>
        </BrowserRouter>
    )
}