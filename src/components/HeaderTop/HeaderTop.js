import React from 'react';

import './header-top.scss';

import Search from './../Search/Search.js';
import Notify from './../Notify/Notify.js';
import Profile from './../Profile/Profile.js';

export default function HeaderTop(props) {
  return(
    <header className="header-top fade-down">
      <h1 className="title">{ props.title }</h1>
      <nav className="nav">
        <Search />
        <Notify />
        <div className="line"></div>
        <Profile />
      </nav>
    </header>
  )
}