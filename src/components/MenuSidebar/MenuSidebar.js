import React from 'react';
import './menu-side-bar.scss';
import Icon from '../../components/Icon/Icon.js';
import { Link } from 'react-router-dom'

export default function MenuSidebar() {
  const routerPath = window.location.pathname;

  return(
    <nav className="menu-side-bar slide-right">
        <Link to="/" className="logo">
          <Icon name="logo" color="white" size="39"/>
        </Link>

      <Link
        to="/"
        className={ routerPath === '/' ? 'link -active' : 'link' }> 
        <Icon name="pie" color="#9FA2B4" size="16" className="icon" />
        <span className="name">Visão Geral</span>
      </Link>

      <Link
        to="/lojas"
        className={ routerPath === '/lojas' ? 'link -active' : 'link' }> 
          <Icon name="ticket" color="#9FA2B4" size="16" className="icon" />
          <span className="name">Lojas</span>
      </Link>

      <Link 
        to="/vendas" 
        className={ routerPath === '/vendas' ? 'link -active' : 'link' }> 
          <Icon name="light" color="#9FA2B4" size="16" className="icon" />
          <span className="name">Vendas</span>
      </Link>

      <Link 
        to="/clientes" 
        className={ routerPath === '/clientes' ? 'link -active' : 'link' }> 
        <Icon name="team" color="#9FA2B4" size="16" className="icon" />
        <span className="name">Clientes</span>
      </Link>

      <Link 
        to="/produtos" 
        className={ routerPath === '/produtos' ? 'link -active' : 'link' }> 
        <Icon name="profile" color="#9FA2B4" size="16" className="icon" />
        <span className="name">Produtos</span>
      </Link>

      <Link 
        to="/planos-e-metas" 
        className={ routerPath === '/planos-e-metas' ? 'link -active' : 'link' }> 
        <Icon name="book" color="#9FA2B4" size="16" className="icon" />
        <span className="name">Planos e Metas</span>
      </Link>

      <hr className="line" />

      <Link 
        to="/configuracoes" 
        className={ routerPath === '/configuracoes' ? 'link -active' : 'link' }> 
        <Icon name="settings" color="#9FA2B4" size="16" className="icon" />
        <span className="name">Configurações</span>
      </Link>

      <Link 
        to="/sair" 
        className={ routerPath === '/sair' ? 'link -active' : 'link' }> 
        <Icon name="badge" color="#9FA2B4" size="16" className="icon" />
        <span className="name">Sair</span>
      </Link>
    </nav>
  )
}