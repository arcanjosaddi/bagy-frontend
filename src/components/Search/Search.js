import React from 'react';

import './search.scss';

import Icon from '../../components/Icon/Icon.js';


export default function Search() {

    return(
        <div className="search">
            <div className="form-search">
                <input type="search" placeholder="Buscar..."/>
            </div>
            <button className="button-search">
                <Icon name="search" color="#C5C7CD" size="16"/> 
            </button>
        </div>
    )
}