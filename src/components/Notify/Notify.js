import React from 'react';

import './notify.scss';

import Icon from '../../components/Icon/Icon.js';

export default function Notify() {

    return(
        <div className="notify">
            <button className="button-notify">
                <div className="alert">
                    <div className="ball pulse-pin"></div>
                </div>
                <Icon name="notify" color="#C5C7CD" size="16"/> 
            </button>
            <div className="notify-list">
                <div className="notify-item --noread">Novo cadastro no sistema</div>
                <div className="notify-item --noread">Nova venda</div>
                <div className="notify-item --noread">Estilo Pri enviou uma mensagem</div>
                <div className="notify-item">Novo cadastro no sistema</div>
                <div className="notify-item">Novo cadastro no sistema</div>
            </div>
        </div>
    )
}