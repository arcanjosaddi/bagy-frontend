import React from 'react';

import './total-sales.scss';

// Components
import SelectComp from '../../components/SelectComp/SelectComp.js';

export default function TotalSales() {

    return(
        <div className="total-sales fade-up">
            <div className="scroll-y">
                <div className="table --fixed">
                    <div className="thead">
                        <div className="tr">
                            <div className="th">Total de compras</div>
                            <div className="td align-right">
                                <SelectComp 
                                    label="Semanal" 
                                    color="pink"
                                    size="small"
                                    data={ ["Semanal", "Mensal", "Anual"] }
                                    />
                            </div>
                        </div>
                        <div className="tr">
                            <div className="td align-left">
                                <span className="info">Valor geral: 13.250,00</span>
                            </div>
                        </div>
                    </div>
                    <div className="tbody">
                        <div className="tr">
                            <div className="th">Estilo Pri</div>
                            <div className="td">250 compras</div>
                            <div className="td align-right">R$ 4238,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Vilma Calçados</div>
                            <div className="td">187 compras</div>
                            <div className="td align-right">R$ 1005,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Mary Lingerie</div>
                            <div className="td">124 compras</div>
                            <div className="td align-right">R$ 914,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Loja Belíssima</div>
                            <div className="td">89 compras</div>
                            <div className="td align-right">R$ 281,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Estilo Pri</div>
                            <div className="td">250 compras</div>
                            <div className="td align-right">R$ 4238,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Vilma Calçados</div>
                            <div className="td">187 compras</div>
                            <div className="td align-right">R$ 1005,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Mary Lingerie</div>
                            <div className="td">124 compras</div>
                            <div className="td align-right">R$ 914,00</div>
                        </div>
                        <div className="tr">
                            <div className="th">Loja Belíssima</div>
                            <div className="td">89 compras</div>
                            <div className="td align-right">R$ 281,00</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}