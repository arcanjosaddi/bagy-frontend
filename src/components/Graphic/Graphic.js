import React from 'react';
import {
    AreaChart,
    Area,
    CartesianGrid,
    XAxis,
    YAxis,
    ResponsiveContainer,
    Tooltip
} from 'recharts';
import { useQuery } from '@apollo/client';

// Query GraphQL
import { STORE_NAME } from './../../services/graphql.js';

// Sass
import './graphic.scss';

// Mocks
import dataGraphic from './data-graphic.js';
import dataMonths from './months.js';
import dataYears from './years.js';

// Components
import SelectComp from '../../components/SelectComp/SelectComp.js';


export default function Graphic() {
    const { loading, error, data } = useQuery(STORE_NAME);
    
    if(error) console.log(error);

    return(
        <div className="graphic fade-up">
            <div className="graphic-body">
                <h2 className="title">Total de faturamento mensal</h2>
                <div className="infos">
                    <div className="month">JULHO 2020</div>
                    <div className="label-graphic">
                        <span className="color color-primary"></span>
                        <span className="label">Este mês</span>
                        <span className="color color-second"></span>
                        <span className="label">Mês passado</span>
                    </div>
                </div>
                <ResponsiveContainer width="100%" height={ 348 }>
                    <AreaChart height={ 348 }  data={ dataGraphic }>
                        <defs>
                            <linearGradient
                                id="colorUv"
                                x1="100%"
                                y1="0%"
                                x2="0%"
                                y2="0%">
                                <stop
                                    offset="0%"
                                    stopColor="transparent"
                                    stopOpacity={ 0.8 }
                                    />
                                <stop
                                    offset="100%"
                                    stopColor="transparent"
                                    stopOpacity={ 0 }
                                    />
                            </linearGradient>
                            <linearGradient
                                id="colorPv"
                                x1="100%"
                                y1="0%"
                                x2="0%"
                                y2="0%">
                                <stop
                                    offset="15%"
                                    stopColor="transparent"
                                    stopOpacity={ 1 }
                                    />
                                <stop
                                    offset="100%"
                                    stopColor="rgba(55,81,255, 1)"
                                    stopOpacity={ .1 }
                                    />
                            </linearGradient>
                        </defs>
                        <XAxis
                            dataKey="name"
                            axisLine={ false }
                            tickLine={ false }
                            tickSize={ 10 }
                            tick={
                                {
                                    fontSize: 10,
                                    backgroundColor: 'white',
                                    stroke: '#9FA2B4',
                                    strokeWidth: .3
                                }
                            }
                            padding={{ right: 40 }}
                            />
                        <YAxis
                            padding={{ top: 60 }}
                            orientation="right"
                            axisLine={ false }
                            tickLine={ false }
                            mirror={ true }
                            tickSize={ 0 }
                            tick={
                                {
                                    fontSize: 10,
                                    stroke: '#9FA2B4',
                                    strokeWidth: .3,
                                }
                            }
                            />
                        <CartesianGrid
                            stroke="#EBEDF0"
                            vertical={ false }
                            />
                        <Tooltip 
                            contentStyle={
                                {
                                    borderRadius: '2px',
                                    border: 'none',
                                    padding: '0 14px',
                                    backgroundColor:'white',
                                    boxShadow: '0 8px 8px rgba(0,0,0, .3)'
                                }
                            }
                            labelStyle={{ display: 'none' }}
                            itemStyle={{ fontSize: 14, fontWeight: 'bold' }}
                            />
                        <Area
                            type="monotone"
                            dataKey="Junho"
                            stroke="#DFE0EB"
                            strokeWidth={ 2 }
                            fillOpacity={ 1 }
                            activeDot={
                                {
                                    stroke: '#DFE0EB',
                                    fill: 'white',
                                    strokeWidth: 4,
                                    r: 5
                                }
                            }
                            fill="url(#colorUv)"
                            />
                        <Area
                            type="monotone"
                            dataKey="Julho"
                            stroke="#FC3C8D"
                            strokeWidth={ 2 }
                            fillOpacity={ 1 }
                            activeDot={
                                { 
                                    stroke: '#FC3C8D',
                                    fill: 'white',
                                    strokeWidth: 4,
                                    r: 5 
                                }
                            }
                            fill="url(#colorPv)"
                            />
                    </AreaChart>
                </ResponsiveContainer>
            </div>
            <div className="graphic-filters">
                <div className="filter">
                    <h3 className="title">Loja</h3>
                    <SelectComp 
                        label={ loading ? 'Carregando...' : data.me.name }
                        data={
                            [
                                "Loja do testinho 1",
                                "Loja do testinho 2",
                                "Loja do testinho 3",
                                "Loja do testinho 4",
                                "Loja do testinho 5",
                            ] 
                        }
                        />
                </div>
                <div className="filter">
                    <h3 className="title">Mês</h3>
                    <SelectComp label="Julho" data={ dataMonths } />
                </div>
                <div className="filter">
                    <h3 className="title">Ano</h3>
                    <SelectComp label="2020" data={ dataYears } />
                </div>
                <div className="filter">
                    <h3 className="title">Total de faturamento</h3>
                    <h4 className="value">R$ 45.000,00</h4>
                </div>
                <div className="filter">
                    <h3 className="title">Análise comparativa</h3>
                    <h4 className="value --success">Positivo</h4>
                </div>
            </div>
        </div>
        )
    }