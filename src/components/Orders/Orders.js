import React from 'react';
import { useQuery } from '@apollo/client';

import './orders.scss';

import { STORE_ORDERS } from './../../services/graphql.js';

function formatDate(date) {
    let orderDate = new Date(date);
    let options = { year: '2-digit', month: '2-digit', day: '2-digit' };

    return orderDate.toLocaleDateString('pt-BR', options);
}


export default function Orders() {
    const { error, data } = useQuery(STORE_ORDERS);
    
    if(error) console.log(error);

    return(
        <div className="orders fade-up">
            <div className="scroll-y">
                <div className="table --fixed">
                    <div className="thead">
                        <div className="tr">
                            <div className="th">Produto</div>
                            <div className="th align-center">Loja</div>
                            <div className="th align-center">Preço</div>
                            <div className="th align-right">Data</div>
                        </div>
                    </div>
                    <div className="tbody">
                        {  
                            data?.getConsolidatedOrders.map((order) => {
                                return  <div className="tr" key={ order.consolidatedOrderId.toString() }>
                                            <div className="th">
                                                { order.products[0].productName }&nbsp;
                                                #{ order.consolidatedOrderId } 
                                            </div>
                                            <div className="td">Estilo Pri</div>
                                            <div className="td">
                                                <span className="label-round --blue">
                                                    R$ { order.price }
                                                </span>
                                            </div>
                                            <div className="td align-right">
                                                <span className="label-round --yellow">
                                                    { formatDate(order.updatedAt) }
                                                </span>
                                            </div>
                                        </div>
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}