import React, { Component } from 'react';

import './select-comp.scss';

export default class SelectComp extends Component {
    renderOption(value){
        return (<div className="option">{value}</div>)
      }
    render() {
        return(
            <div className={ this.props.color ? `select --${this.props.color}` : 'select'}>
                <div className={ this.props.size ? `label --${this.props.size}` : 'label'}>
                    { this.props.label }
                </div>
                <div className={ this.props.size ? `options --${this.props.size}` : 'options'}>
                    {   
                        this.props.data !== undefined
                        ?
                            this.props.data.map((value) => {
                                return <div key={value.toString()}
                                            className="option">
                                            {value}
                                        </div>
                            })
                        :
                            ''
                    }
                </div>
            </div>
        )
    }
}