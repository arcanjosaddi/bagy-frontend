import React from 'react';

import './card.scss';

export default function Card(props) {
    return(
        <div className="fade-up">
            <div className="card" title={ props.value }>
                <h3 className="title limit-one-line">{ props.title }</h3>
                <h2 className="value limit-one-line">{ props.value }</h2>
            </div>
        </div>
    )
}