import React, { Component } from 'react';

import './profile.scss';

import profile from './../../assets/images/profile.png';

export default class Profile extends Component {
    render() {
        return(
            <div className="profile">
                <h4 className="name">Matheus Borges</h4>
                <div className="image">
                    <img src={ profile } alt=""/>
                </div>
            </div>
        )
    }
}