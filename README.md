# DASHBOARD IN REACT.JS

This simple dashboard made in React.js.

Main technologies used: `React.Js, GraphQL, Apollo/Client, Yarn.`

## QUICK INSTALL:

### Pre Requisite:

- Git.
- Yarn or Npm.
- NodeJs.

## Clone Project:

In your terminal execute this commands:

```bash
$ git clone https://arcanjosaddi@bitbucket.org/arcanjosaddi/bagy-frontend.git
```

## Install Dependencies:

In your terminal:

```bash
$ cd bagy-frontend

$ yarn install
or
$ npm install
```

## Running:

You can start running:

```bash
$ yarn start
or
$ npm run start
```
## DEPENDENCIES:

### LIST OF REQUIRE DEPENDENCIES:

- [facebook/react](https://github.com/facebook/react): A JavaScript library for building user interfaces
- [sass/node-sass](https://github.com/sass/node-sass): Node-sass is a library that provides binding for Node.js to [LibSass](https://github.com/sass/libsass), the C version of the popular stylesheet preprocessor, Sass.
- [graphql/graphql-js](https://github.com/graphql/graphql-js): The JavaScript reference implementation for GraphQL, a query language for APIs created by Facebook.
- [apollographql/apollo-client](https://github.com/apollographql/apollo-client): Apollo Client is a fully-featured caching GraphQL client with integrations for React, Angular, and more. It allows you to easily build UI components that fetch data via GraphQL.
-  [recharts/recharts](https://github.com/recharts/recharts): Recharts is a **Redefined** chart library built with [React](https://facebook.github.io/react/) and [D3](http://d3js.org/).

